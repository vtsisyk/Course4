#include <QApplication>

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QPointF>
#include <QVector>
#include <QBrush>
#include <QPen>
#include <QVector4D>
#include "affinematrix.h"
inline void transform(double* x, double* y)
{
    double tx = *x;
    *x = *x * 1.5 + *y * 0 + 0;
    *y = *x * 0 + *y * 1.5 + 0;
}
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QVector<QVector4D> points;

    QVector<QPointF> po;


    QGraphicsView * view = new QGraphicsView();
    QGraphicsScene * scene = new QGraphicsScene();
    view->setScene(scene);

    QPen pen;
    AffineMatrix test;
    test.push_back(10,60,60);
    test.push_back(30,10,80);
    test.push_back(20,40,30);
    test.push_back(30,80,30);
    test.push_back(60,30,60);


    int x,y,z;
    for(int i = 0; i< test.points.size(); i++){
        x = test.points[i].x();
        y = test.points[i].y();

        scene->addEllipse(x, y, 12,12, pen, QBrush(QColor("#DDFFAA")));
    }



    //test.transformAll(2);
    test.RotateAllZ(45 * (3.14/180));
   // test.to2D();
    for(int i = 0; i< test.points.size(); i++){
        x = test.points[i].x();
        y = test.points[i].y();

        scene->addEllipse(x, y, 12,12, pen, QBrush(QColor("#FF44AA")));
    }
    // Show the view
    view->show();

    // or add the view to the layout inside another widget

    return a.exec();
}


