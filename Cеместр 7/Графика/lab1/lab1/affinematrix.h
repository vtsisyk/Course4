#ifndef AFFINEMATRIX_H
#define AFFINEMATRIX_H

#include <QObject>
#include <math.h>
#include <QVector4D>
#include <QVector2D>
#include <QVector>

class AffineMatrix
{
public:
    QVector<QVector4D> points;
    QVector<QVector2D> points2D;
public:
    void to2D();
    void push_back(int x, int y, int z);
    void transformAll(double mag);
    inline void transform(double* x, double* y, double *z, double mag);
    void RotateAllX(double phi);
    void RotateAllY(double phi);
    void RotateAllZ(double phi);
    inline void rotateX(double* x, double* y, double *z, double phi);
    inline void rotateY(double* x, double* y, double *z, double phi);
    inline void rotateZ(double* x, double* y, double *z, double phi);

    AffineMatrix();
};

#endif // AFFINEMATRIX_H
