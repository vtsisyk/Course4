#include "affinematrix.h"
AffineMatrix::AffineMatrix()
{

}


void AffineMatrix::push_back(int x, int y, int z)
{
    points.push_back(QVector4D(x,y,z,1));
}

void AffineMatrix::to2D()
{
    points2D.clear();
    int x,y,z;
    for(int i = 0; i < points.size(); i++){
        x = points[i].x();
        y = points[i].y();
        z = points[i].z();

        x = x / z;
        y = y / z;
        points2D.push_back(QVector2D(x,y));
    }
}

void AffineMatrix::transformAll(double mag)
{
    double x, y, z;
    for(int i = 0; i < points.size(); i++){
        x = points[i].x();
        y = points[i].y();
        z = points[i].z();
        transform(&x, &y, &z, mag);

        points[i].setX(x);
        points[i].setY(y);
        points[i].setZ(z);
    }

}


void AffineMatrix::RotateAllX(double phi)
{
    double x, y, z;
    for(int i = 0; i < points.size(); i++){
        x = points[i].x();
        y = points[i].y();
        z = points[i].z();
        rotateX(&x, &y, &z, phi);

        points[i].setX(x);
        points[i].setY(y);
        points[i].setZ(z);
    }

}

void AffineMatrix::RotateAllY(double phi)
{
    double x, y, z;
    for(int i = 0; i < points.size(); i++){
        x = points[i].x();
        y = points[i].y();
        z = points[i].z();
        rotateY(&x, &y, &z,phi);

        points[i].setX(x);
        points[i].setY(y);
        points[i].setZ(z);
    }

}
void AffineMatrix::RotateAllZ(double phi)
{
    double x, y, z;
    for(int i = 0; i < points.size(); i++){
        x = points[i].x();
        y = points[i].y();
        z = points[i].z();
        rotateZ(&x, &y, &z, phi);

        points[i].setX(x);
        points[i].setY(y);
        points[i].setZ(z);
    }

}
inline void AffineMatrix::transform(double* x, double* y, double *z, double mag)
{
    *x = *x * mag;
    *y = *y * mag;
    *z = *z * mag;

}
inline void AffineMatrix::rotateX(double* x, double* y, double *z, double phi)
{

    *y = *y * cos(phi)+ *z *(-1) * sin(phi);
    *z = *y * sin(phi)+ *z * cos(phi);
}

inline void AffineMatrix::rotateY(double* x, double* y, double *z, double phi)
{

    *x = *x * cos(phi)+ *z * sin(phi);
    *z = *x *(-1)* sin(phi)+ *z * cos(phi);
}
inline void AffineMatrix::rotateZ(double* x, double* y, double *z, double phi)
{

    *x = *x * cos(phi)+ *y *(-1)* sin(phi);
    *y = *x * sin(phi)+ *y * cos(phi);
}

