#include <iostream>
#include <vector>
#include <queue>
#include <string.h>
class Vertex {
	private:
		int index;    /* верхняя часть */
		int t_early;  /* левая часть */
		int t_lately; /* правая часть */
		int diff;     /* нижняя часть */
		int priority; /* сколько событий наступило до этого */
		std::vector<std::pair<int, int>> edges; /* Вес ребра и куда оно идет */
		std::vector<std::pair<int, int>> backedges; /* "обратные" ребра */
	public:
		Vertex();
		int addEdge(int, int);
		int addBackEdge(int, int);
		int canGo(bool *);
		int getPriority() { return priority; }
		void setPriority(int pr) { priority = pr; }
		int getEarly() {return t_early;}
		int getLately() {return t_lately;}
		void setEarly(int t) {t_early = t;}
		void setLately(int t) {t_lately = t;}
		int getIndex() {return index;}
		void setIndex(int indx) {index = indx;}
		int sizeOfEdges();
		void printPaths();
		int sizeOfBackEdges() {return backedges.size();}
		std::pair<int, int> getEdge(int i) {return edges[i];}
		std::pair<int, int> getBackEdge(int i) {return backedges[i];}
		static void bfsltr(Vertex *beginning, Vertex *vertexes);
		static void bfsrtl(Vertex *ending, Vertex *vertexes);
};

Vertex::Vertex()
{
	index = t_early =  diff = priority = 0;
	t_lately = 1000;
}

int Vertex::addEdge(int weight, int where)
{
	std::pair<int,int> new_edge(weight, where);
	edges.push_back(new_edge);

	return 0;
}

int Vertex::addBackEdge(int weight, int where)
{
	std::pair<int,int> back_edge(weight, where);
	backedges.push_back(back_edge);

	return 0;
}
int Vertex::sizeOfEdges()
{
	return edges.size();
}

int Vertex::canGo(bool *used)
{
	int size = edges.size(); 
	for (int i = 0; i < size; i++) {
		used[edges[i].second] = 1;
	}
	return size;
}

void Vertex::bfsltr(Vertex *beginning, Vertex *vertexes)
{
	std::queue<Vertex *> q;
	Vertex *current;
	q.push(beginning);
	
	while (!q.empty()) {
		current = q.front();
		q.pop();
		
		for (int i = 0; i < current->sizeOfEdges(); i++) {
			std::pair<int,int> p = current->getEdge(i);
			if (vertexes[p.second].getEarly() < current->getEarly() + p.first) {
				vertexes[p.second].setEarly(current->getEarly() + p.first);
				vertexes[p.second].setPriority(current->getPriority() + 1);
				q.push(&vertexes[p.second]);
			}
		}
	}

}

void Vertex::bfsrtl(Vertex *ending, Vertex *vertexes)
{
	std::queue<Vertex *> q;
	Vertex *current;
	q.push(ending);
	
	while (!q.empty()) {
		current = q.front();
		q.pop();
		std::cout <<"+++ " <<current->index <<"--" << current->sizeOfBackEdges()<< " +++ \n";
		for (int i = 0; i < current->sizeOfBackEdges(); i++) {
			std::pair<int,int> p = current->getBackEdge(i);
			std::cout <<"----" << vertexes[p.second].getLately() << "---" << current->getLately() - p.first<< "\n";
			if (vertexes[p.second].getLately() > current->getLately() - p.first) {
				vertexes[p.second].setLately(current->getLately() - p.first);
				q.push(&vertexes[p.second]);
			}
		}
	}

}

void Vertex::printPaths()
{
	for (int i = 0; i < sizeOfEdges(); i++) {
			std::pair<int,int> p = getEdge(i);
			std::cout<< "откуда: " << index 
				     << " t_раннее: "<< p.first
				     << " куда: " << p.second
				     << "\n";
	}
}

int comp(const void *a, const void *b)
{
	if (((Vertex *) a)->getPriority() < ((Vertex *) b)->getPriority() )
		return 0;
	return 1;

}
int main(int argc, const char *argv[])
{
	int size;
	std::cout << "Введите количество вершин: ";
	std::cin >> size;

	Vertex *vertexes = new Vertex[size];
	/* ввод исходных данных */
	while (true) {
		int from, weight, where;
		std::cout << "откуда идет ребро -  вес - куда(выход - (-1))\n";
		std::cin >> from;
		if (from == -1) {
			break;
		}
		// TODO: проверить входные данные на достоверность 
		std::cin >> weight >> where;
		vertexes[from].addEdge(weight, where);
		vertexes[where].addBackEdge(weight, from);
		vertexes[from].setIndex(from);
		vertexes[where].setIndex(where);
	}
	
	bool *used = new bool[size];
	memset(used, 0, sizeof(bool)*size);
	Vertex *beginning, *ending;
	beginning = ending = NULL;
	int retval;
	/*
	 * Проходим по всем ребрам. Если из одной вершины можно перейти в другую, то
	 * очевидно, что эта другая вершина не будет начальной. Вершины, в
	 * которые можно попасть мы отмечаем. Мы должны найти такую
	 * вершину, в котороую не ведет ни одно ребро. Такая вершина будет обозначена
	 * нулем в массиве used. 
	 * Если же из вершины нельзя перейти ни в какую другю вершину, то она 
	 * является конечной.
	 */
	for (int i = 0; i < size; i++) {
			retval = vertexes[i].canGo(used);
			if (retval == 0) {
				std::cout << "конец: ";
				std::cout << i << "\n";
				ending = &vertexes[i];
			}
	}

	for (int i = 0; i < size; i++) {
		if (used[i] == 0) {
			std::cout << "начало: ";
			std::cout << i << "\n";
			beginning = &vertexes[i];
			break;
		}
	}
	/* Делаем обход в ширину. Мы проходим по всем ребрам, и, если t_раннее
	 * больше чем то, что имеется в данной вершине, то мы обновляем значение */
	Vertex::bfsltr(beginning, vertexes);

	ending->setLately(ending->getEarly());
	Vertex::bfsrtl(ending, vertexes);

	for (int i = 0; i < size; i++) {
		std::cout << "i: " << vertexes[i].getIndex()
				  << " left: " << vertexes[i].getEarly()
				  << " right: "<< vertexes[i].getLately() 
				  <<" priority: "<< vertexes[i].getPriority()
				  <<"\n";
	}
	Vertex *sorted(vertexes);

	std::cout << "\n----\n";
	qsort(sorted, size, sizeof(Vertex), comp);
	for (int i = 0; i < size; i++) {
		std::cout << "i: " << sorted[i].getIndex()
				  << " left: " << sorted[i].getEarly()
				  << " right: "<< sorted[i].getLately() 
				  <<" priority: "<< sorted[i].getPriority()
				  <<"\n";
	}
	for (int i = 0; i < size; i++) {
		sorted[i].printPaths();
	}

	delete[] vertexes;
	delete[] used;
	return 0;
}
